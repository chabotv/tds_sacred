This depository contains minimal example to do a few experiments with `sacred`. 
To run the experience using neptune observer you need to create a Neptune account (https://ui.neptune.ml/) and add Neptune token in your bashrc 
```sh
export NEPTUNE_API_TOKEN=your_token
```