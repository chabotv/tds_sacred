import numpy.random as random
from sklearn import svm, datasets
from sacred import Experiment

from sacred.observers import FileStorageObserver
import pickle 
import os 
import logging 
logger = logging.getLogger('iris_test')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
logger.addHandler(ch)

ex = Experiment('test_rbf_svm')
try:
    from neptunecontrib.monitoring.sacred import NeptuneObserver
    from neptune import api_exceptions
    try:
        ex.observers.append(NeptuneObserver(project_name='vchabot/TDS-sacredDemo'))
    except api_exceptions.ProjectNotFound as e:
        logger.warning("Netpune observer not added : project was not found. Error is %s"%repr(e))
    except Exception as e:
        logger.warning("An unregistered exception prevent to use neptune observer : %s"%repr(e))
except ModuleNotFoundError:
    logger.warning("Neptune observer or neptune module was not found. Please check your installation.")

ex.observers.append(FileStorageObserver.create("./Iris_Test/"))

@ex.config
def cfg():
    """
    Basic configuration
    """
    C = 1.0
    gamma = 0.7
    opt = {}
    to_print={"C":C,
	      "gamma":2.5}
          
@ex.named_config
def cfg2():
    """
    Very low gamma test
    """
    gamma = 0.001

@ex.capture(prefix='to_print')
def print_param(C,gamma):
    """
    Printing parameter in to_print dictionnary. 
    """
    print(C)
    print(gamma)

@ex.automain
def run(C, gamma, opt, _run):
    print_param()
    iris = datasets.load_iris()
    per = random.permutation(iris.target.size)
    iris.data = iris.data[per]
    iris.target = iris.target[per]
    clf = svm.SVC(C, 'rbf', gamma=gamma)
    clf.fit(iris.data[:90],
            iris.target[:90])
    # Save the model if required 
    if opt.get("save_model",False):
        tmp_dir = "./Iris_Test/tmp/"
        os.makedirs(tmp_dir,exist_ok=True)
        with open(tmp_dir + "fit_svm_iris.pkl","wb") as fp:
            pickle.dump(clf,fp)
        _run.add_artifact(tmp_dir + "fit_svm_iris.pkl", name="model_fit.pkl")
        os.remove(tmp_dir + "fit_svm_iris.pkl")

    return clf.score(iris.data[90:],
                     iris.target[90:])
